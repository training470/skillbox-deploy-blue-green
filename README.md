# Порядок выполнения заданий
## Задание 6 Terraform + Ansible
### Запуск Terraform
1. Переименовать файл secret.tfvars.dist в secret.tfvars, заполнить учетными данными для доступа к yandex cloud
2. Инициализация и запуск виртуальной машины
```bash
terraform init
terraform plan -var-file="secret.tfvars"
terraform apply -var-file="secret.tfvars"
```  
Завершение работы виртуальной машины
```bash
terraform destroy -var-file="secret.tfvars"
``` 
Зайти на виртуальную машину (ip-адрес можно увидеть в логе или в личном кабинете провайдер услуг виртуальной машины
```bash
ssh -i ~/.ssh/yandex-terraform1 ubuntu@51.250.2.151
```
3. Деплой приложения
```bash
cd scripts
ansible-playbook -i hosts.yml -b reactjs.yml -vv
```


## Действия на виртуальном сервере
```bash
$ # регистрируем раннер
$ sudo mkdir /var/www/test-app
$ sudo vim /srv/gitlab-runner/config/config.toml
```
```ruby
[session_server]
  session_timeout = 1800

[[runners]]
  name = "test-react"
  url = "https://gitlab.com/"
  token = "YAmAa2TFNQTXifbNJj38"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "ruby:2.7"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache", "/var/www/:/www:rw"]  # Добавляем "/var/www/:/www:rw" для монтирования папки в контейнере
    shm_size = 0

[[runners]]
  name = "skillbox-deploy-blue-green"
  url = "https://gitlab.com/"
  token = "vq1y1y3e5RpixfnWHUhy"
  executor = "docker"
  [runners.custom_build_dir]
```
Настариваем ссылки
```bash
sudo rm -rf /var/www/html  # Для начала удаляем прежнюю версию сайта
```
Вносим изменения в .gitlab-ci.yml
```yaml
test:
  stage: deploy
  script:
    - cp -r build /www/test-app/$CI_COMMIT_SHA
    - ln -fsnv /var/www/test-app/$CI_COMMIT_SHA /www/html
```

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `yarn build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
