terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

variable "yandex_token" {}
variable "yandex_cloud_id" {}
variable "yandex_folder_id" {}

// Configure the Yandex.Cloud provider
provider "yandex" {
  token     = var.yandex_token
  cloud_id  = var.yandex_cloud_id
  folder_id = var.yandex_folder_id
  zone      = "ru-central1-a"
}

// Create a new instance
resource "yandex_compute_instance" "vm-1" {
  name = "terraform1"
  resources {
    cores  = 2
    memory = 2    
  }

  boot_disk {
    initialize_params {
      image_id = "fd8fte6bebi857ortlja"
      size = 10
    }    
  }

#  tags = {
#    Name = "ReactJS Server IP"
#    Env = "Production"
#    Tier = "Frontend"
#  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/yandex-terraform1.pub")}"
  }
  
  provisioner "file" {
    source      = "scripts/create.sh"
    destination = "/tmp/create.sh"
    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file("~/.ssh/yandex-terraform1")}"
      host        = yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
    }
  }

  # Могут быть ошибки чтения файла, исправить sed -i -e 's/\r$//' create.sh
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/create.sh",
      "/tmp/create.sh",
    ]
    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file("~/.ssh/yandex-terraform1")}"
      host        = yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
    }
  }  
  
  lifecycle {
    create_before_destroy = true
  }

}

resource "yandex_vpc_network" "network-1" {
  name = "network1"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet1"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

output "internal_ip_address_vm_1" {
  value = yandex_compute_instance.vm-1.network_interface.0.ip_address
}

output "external_ip_address_vm_1" {
  value = yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
}
