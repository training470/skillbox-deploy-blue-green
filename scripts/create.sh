#!/bin/sh

sudo su -c "exec > >(tee /var/log/create.log|logger -t create.sh -s 2>/dev/console) 2>&1"

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update -y && sudo apt install yarn -y
sudo apt install nodejs -y
sudo apt install npm -y
npm install
sudo apt install git -y
sudo apt install nginx -y

# We can get the IP address of instance
# myip=`curl http://169.254.169.254/latest/meta-data/local-ipv4`